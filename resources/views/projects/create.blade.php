@extends('layouts.app')

@section('content')
    <div class="w-full md:w-3/5 bg-white shadow rounded-lg px-20 py-12 mx-auto mt-6">
        <h1 class="text-center text-3xl text-black font-normal mb-10">Let's start something new</h1>

        <form method="POST" action="/projects">
            @include('projects.form', [
                'project' => new App\Project,
                'buttonText' => 'Create project'
            ])
        </form>
    </div>
@endsection
