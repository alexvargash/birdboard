@csrf
<div class="flex flex-col mb-8">
    <label class="text-base text-black mb-3">Title<span class="text-blue-dark">*</span></label>
    <input type="text" name="title" placeholder="My next awesome project" class="w-full border border-grey rounded p-3" value="{{ $project->title }}">
</div>

<div class="flex flex-col mb-8">
    <label class="text-base text-black mb-3">Description<span class="text-blue-dark">*</span></label>
    <textarea name="description" placeholder="I should start ..." class="w-full h-32 border border-grey rounded p-3">{{ $project->description }}</textarea>
</div>

<div class="flex items-center justify-start">
    <button type="submit" class="button text-base">{{ $buttonText }}</button>
    <a href="{{ $project->path() }}" class="text-sm text-grey ml-4 no-underline hover:underline hover:text-red">Cancel</a>
</div>

@if ($errors->any())
    <div class="flex flex-col mt-8">
        <ul class="list-reset">
            @foreach ($errors->all() as $error)
                <li class="text-sm text-red">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
