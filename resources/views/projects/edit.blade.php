@extends('layouts.app')

@section('content')
    <div class="w-full md:w-3/5 bg-white shadow rounded-lg px-20 py-12 mx-auto mt-6">
        <h1 class="text-center text-3xl text-black font-normal mb-10">Edit your project</h1>

        <form method="POST" action="{{ $project->path() }}">
            @method('PATCH')
            @include('projects.form', [
                'buttonText' => 'Update project'
            ])
        </form>
    </div>
@endsection
