<?php

namespace Tests\Setup;

use App\Task;
use App\User;
use App\Project;

class ProjectFactory
{
    /**
     * User that is owner of the project.
     * @var App\User
     */
    protected $user = null;

    /**
     * Number of task associated with the project.
     * @var integer
     */
    protected $taskCount = 0;

    /**
     * Set the user asociated with the project.
     *
     * @param  App\User $user
     * @return ProjectFactory $this
     */
    public function ownedBy($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set the number of task asociated with the project.
     *
     * @param  integer $count
     * @return ProjectFactory $this
     */
    public function withTasks($count)
    {
        $this->taskCount = $count;

        return $this;
    }

    /**
     * Create a new project with it's owner, and create the tasks needed for
     * that project.
     *
     * @return App\Project $project
     */
    public function create()
    {
        $project = factory(Project::class)->create([
            'owner_id' => $this->user ?? factory(User::class)
        ]);

        factory(Task::class, $this->taskCount)->create([
            'project_id' => $project->id
        ]);

        return $project;
    }
}
