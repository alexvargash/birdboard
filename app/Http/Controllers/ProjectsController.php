<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProjectRequest;

class ProjectsController extends Controller
{

    /**
     * Return all the project of the logged in user.
     *
     * @return view projects\index
     */
    public function index()
    {
        $projects = auth()->user()->projects;

        return view('projects.index', compact('projects'));
    }

    /**
     * Show a single project.
     *
     * @param  Project $project
     * @return view projects\show
     */
    public function show(Project $project)
    {
        $this->authorize('view', $project);

        return view('projects.show', compact('project'));
    }

    /**
     * Show the create form for a new project.
     *
     * @return view projects\create
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Save a new project for the logged in user.
     *
     * @param  StoreProjectRequest $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function store(StoreProjectRequest $request)
    {
        $project = auth()->user()->projects()->create($request->validated());

        return redirect($project->path());
    }

    /**
     * Show the edit form for an existing project.
     *
     * @return view projects\edit
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update an existing project.
     *
     * @param  StoreProjectRequest $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function update(StoreProjectRequest $request, Project $project)
    {
        $this->authorize('update', $project);

        $project->update($request->validated());

        return redirect($project->path());
    }
}
